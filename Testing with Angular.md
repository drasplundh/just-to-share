### Testing with Angular

---

## Jasmine
- **describe(string, function)** is a function that defines what we call a *Test Suite*
- **it(string, function)** is a unit test, defines an individual *Test Spec*

## Example
```javascript
function helloWorld() {
    return 'Hello World!';
}

// Test

describe('Hello world', () => {
    it('says hello', () => {
        expect(helloWorld())
            .toEqual('Hello World');
    });
});
```

- Jasmine has a lot of built in matchers

## Rules to writing tests
1. Determine and mock dependencies
2. fix failing tests (if any)
3. think about functionality and determine responsibilities
4. Write small and descriptive tests

## How to Run Tests in Angular
- can run `npm test`
    - actually calls a function in `package.json` which runs `ng test`
- When test is run, a browser page will open

## How to set up Karma to show test results in VS Code terminal
- go to `karma.conf.js` file in navigator
- look for `browsers: ['Chrome']`
- change the value to `'ChromeHeadless'`
- Now tests should show up in terminal
 
## Mocking Dependencies
- navigate to `app.component.spec.ts` file
- just below the import statements, you can declare the Component
    - let's say we're mocking a button component
``` javascript
@Component({
    selector: 'app-button',  // This must share the same selector with the actual Component
    template: ''
})
class MockButtonComponent{}

@Component({
    selector: 'app-quote',  // This must share the same selector with the actual Component
    template: ''
})
class MockQuoteComponent{}

// This is a mock Service
class MockQuoteService{
    getOfficeQuote() {}  // This is a method within the QuoteService class
}

```

## TestBed
- Is a module, you must declare your Mock components inside 
``` javascript
describe('AppComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            // Specifically for Modules
            imports: [
                RouterTestingModule
            ],
            // Specifically for Components
            declarations: [
                AppComponent,
                MockButtonComponent // This is our mock declaration
                MockQuoteComponent // This is our mock declaration
            ],
            // Specifically for Services
            providers: [
                {provide: QuoteService, useClass: MockQuoteService}  // This is how to use a mock service
            ]
        }).compileComponents();
    });
// More TestBed code below
```

## Mocking HttpClient
- Angular has a built-in module for testing HttpClient
- You can mock it yourself for total control instead
- navigate to your `yourServiceName.service.spec.ts` file
    - in the example we're using `QuoteService`
``` javascript
import {TestBed} from '@angular/core/testing`;
import [HttpClientTestingModule} from '@angular/common/http/testing'; // Be sure to import here

import {QuoteService} from './quote.service';

describe('QuoteService', () => {
    let service: QuoteService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule] // Built into angular, be sure to include here
        });
        service = TestBed.inject(QuoteService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
```

## Writing Tests/Test Structure
- refer to step #3 (think about functionality and responsibility)
- Think *Given* *When* *Then* or *Arrange* *Act* *Assert*
- **use good long descriptive names**, they're useful in the debugger

# Example
- Given a scenario, where when a Button is clicked, have it execute a function, in our case, "emit"
- a *Spy* is something that returns a *Watcher* which will allow us to make assertions

``` javascript
it('should emit event when button is clicked', () => {  // the first argument is the name of the test, the second is the callback function
    // Arrange
    const spy = spyOn(component.buttonClickEvent, 'emit') // the second parameter is the name of the function you want to spy on
    // Act
    component.onButtonClick(); // actual invocation of the method
    // Assert
    expect(spy).toHaveBeenCalled(); // asserts if the method was called or not
})
```
- Assuming the test is written correctly, it should pass
- in Red/Green testing, we want to verify the test is working as intended
- we can go and change something in the actual code (NOT the test code) to make it so our test fails
- if it does, we can more or less conclude it's behaving as expected

## Checking to see if the DOM updates
- will be using a function called `fixture`
# Example
``` html
<!-- Given -->
<div class="button_container" (click)="onButtonClikc()">
    <div class="button_container-content">
        {{buttonText}}
    </div>
</div>
```

``` javascript

it('should set button text properly if provided and render text on screen', () => {

// Arrange
component.buttonText = 'Testing';
expect(component.buttonText).toBe('Testing'); // simply checks that the variable has been set to 'Testing', it's not super necessary

// NOTE: change detection does not happen automatically, use fixture
fixture.detectChanges(); // looks for changes made to the DOM

/* Act - doesn't really have an act this time */
// holds an instance of the button
// Trying to look at HTML of our elements
//.queryAll looks for an Element on the DOM, we're doing this by CSS
// looks for an element with class name 'button_container'
// .queryAll returns an array of all the elements with class name 'button_container"
// in our case, there's only one, that's why we're using [0].nativeElement
const buttonEl = fixture.debugElement.queryAll(By.css('.button_container'))[0].nativeElement;

expect(buttonEl.innerHTML).toContain('Testing');
});
```

## Testing a Service and HTTP request
- Given the "Office Quote" from the video
``` javascript

const mockQuoteQuote: Quote = {
    id: 'one',
    content: 'Quote here', 
    character: mockCharacter
}
describe('QuoteService', () => {
    let service: QuoteService;
    let httpMock: HttpTestingController; // Necessary for mocking Http requests

    beforeEach(() =>  {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });
        service = TestBed.inject(QuoteService);
        httpMock = TestBed.inject(HttpTestingController);  // creates a new instance of HttpTestingController for each test
    })

}

it('should return dummy quote data when endpoint is called', (done: DoneFn) => { // This let's the testframework know when we're done with an asynchronous function
    //Arrange
    // working with an HTTP request, through observables, so it's ASYNCHRONUS
    // There's no way for the test framework to know when the operation is complete
    // In the actual test function, we use a 'done: DoneFn'
    service.getOfficeQuote().subscribe(quote => {
        //Assert
        expect(quote).toEqual(mockQuote);
        done();
    });

    const req = httpMock.expectOne('https://www.somewebsite.com/api/quotes/random'); // this is the endpoint you want to hit

    //Act
    req.flush(mockQuote);  // sends data down an asynchronus stream
});
